from projectTest.TestServer import app


class TestRequest:
    def setup_class(self):
        self.app = app.test_client()

    def test_helloworld(self):
        res: bytes = self.app.get("/").data
        assert res.decode() == "Hello world!"

    def test_simple_post(self):
        raise Exception("未完成")

    def test_simple_get(self):
        raise Exception("未完成")

    def test_delete(self):
        raise Exception("未完成")

    def test_simple_put(self):
        raise Exception("未完成")
