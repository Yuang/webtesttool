from projectTest.TestServer import app


class TestSmokeRun:
    def test_simple(self):
        pass


class TestWebAppInit:

    def setup_class(self):
        print("开始函数")
        self.appClient = app.test_client().__enter__()
        pass

    @staticmethod
    def teardown_class():
        print("结束函数")
        pass

    def test_hello_world(self):
        res: bytes = self.appClient.get("/").data
        assert res.decode() == "Hello world!"
