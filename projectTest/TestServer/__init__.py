import json

from flask import Flask, request

app = Flask(__name__)


@app.route("/")
def hello_world():
    return "Hello world!"


@app.route("/smoke_test", methods=["GET", "POST", "DELETE", "PUT"])
def smoke_method_test():
    return json.dumps({"method": request.method})
    # pass


if __name__ == "__main__":
    app.run()
