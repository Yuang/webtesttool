import sys

from PyQt5.QtWidgets import QApplication, QWidget


# from PyQt5.QtWidgets import QLineEdit, QFormLayout
# from PyQt5.QtGui import QIntValidator, QDoubleValidator, QFont
# from PyQt5.QtCore import Qt


class LineEditDemo(QWidget):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("test")
        # self.setGeometry(100,100,100,100)
        self.show()


if __name__ == "__main__":
    app = QApplication(sys.argv)
    win = LineEditDemo()
    win.show()
    sys.exit(app.exec_())
    pass
