"""
这里放的是一个运行程序目录,
主要是Python需要指定解释器,Java需要指定Java_Home等一系列变量,
所以我决定写一个接口类解决这个问题.
"""


class Executor:
    def __init__(self, executor_path: str, env_var: map):
        self.executor_path: str = executor_path
        self.env: map = env_var

    def init_env(self):
        pass

    def set_executor_path(self, path: str):
        self.executor_path = path
