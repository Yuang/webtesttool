import os
# import threading
from typing import Optional, Mapping

from AutoTest.Model.WebWithProcess.Executor import Executor

"""
这个类的用处是管理线程.
这里比较倾向于使用命令行来启动web进程而不是通过python对象启动,这样有更好的兼容性.
由于windows和linux的命令行不同,我比较倾向于写一个接口类再对应到两个操作系统.
注意,为了设计的简洁,这里只能额外启动webservice一个线程.
"""


class WebProcess:
    def __init__(self):
        self.working_dir: Optional[str] = None
        self.executor: Optional[Executor] = None
        self.command: Optional[str] = None
        self.env: Mapping[str:str] = None
        pass

    def set_dir(self, path: str):
        self.working_dir = path

    def set_command(self, command: str):
        self.command = command

    def process(self):
        # ---------------------------------
        # 初始化环境
        os.chdir(self.working_dir)
        self.executor.init_env()
        for k, v in self.env:
            os.environ[k] = v
        os.system(self.command)

    # 线程开始运行,但并不阻塞.
    def run(self):
        pass

    # 终止线程运行,可视为发送ctrl-c
    def stop(self):
        pass

    # 阻塞,等待线程执行后退出.
    def join(self):
        pass
