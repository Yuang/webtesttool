import os
from typing import Dict, Optional

from AutoTest.Model.TypeCheck import type_check


class AbstructConfig:
    """抽象的配置文件,不可以被实例化使用."""


class ProjectConfig(AbstructConfig):
    """项目的总配置"""


class ExecutorConfig(AbstructConfig):
    """一个执行器的配置"""

    @type_check
    def __init__(self, home_path: str, bin_path: str, executor_name: str):
        self.home_path: str = home_path
        self.bin_path: str = bin_path
        self.executor_name: str = executor_name
        self.envs: Dict = {}
        self.verion: Optional[str] = None

    @type_check
    def add_env(self, k: str, v: str):
        self.envs[k] = v

    @type_check
    def add_env_set(self, env_set: Dict):
        for k in env_set:
            self.envs[k] = env_set[k]

    def get_executor_path(self) -> str:
        return os.path.join(self.bin_path, self.executor_name)


class PythonExecutorConfig(ExecutorConfig):
    """Python解释器的配置"""

    @type_check
    def __init__(self, home_path: str, bin_path: str):
        super().__init__(home_path=home_path, bin_path=bin_path)


class JavaExecutorConfig(ExecutorConfig):
    """JDK解释器"""
