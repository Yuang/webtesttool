from typing import Optional

import urllib3

import AutoTest.Model.Obj.Request.RequestEncoding as RequestEncoding
# import AutoTest.Model.Obj.Request.RequestBodyType as RequestBody
from AutoTest.Model.Obj.Request.RequestBodyType import RequestBodyType
from AutoTest.Model.Obj.Request.RequestHeaders import RequestHeaders
from AutoTest.Model.Obj.Request.RequestURLParam import RequestUrlParam
from AutoTest.Model.Obj.common.MapObj import DictObject


class RequestTemplate:
    def __init__(self):
        self.method = None  # 后期更改为枚举类,True or False
        self.url: str = ""
        self.urlParam: RequestUrlParam = RequestUrlParam()
        self.headers: RequestHeaders = RequestHeaders()
        self.body: RequestBodyType = RequestBodyType()
        self.encoding: Optional[RequestEncoding] = None
        self.result = None
        self.response = None
        self.httpManager: Optional[urllib3.PoolManager] = None

    # -----------------------------------------------------------
    # 这里我认为使用装配模式更好.
    # ----------------------------------------------------------
    def method_set_get(self):
        self.method = "GET"

    def method_set_post(self):
        self.method = "POST"

    def url_set(self, url: str):
        self.url = url

    def url_param_set(self, url_param):
        self.url = url_param

    def headers_set(self, headers):
        self.headers = headers

    def header_append(self, header: DictObject):
        self.headers[header.key] = header

    # -----------------------------------------------------------
    # 在生命周期中的函数

    # 装配函数
    def build(self):
        pass

    def get_final_url(self):
        tmp = self.url
        for k, v in self.urlParam:
            k: str
            v: DictObject
            tmp = tmp + k + v.value
        return tmp
        pass

    def get_final_headers(self):
        tmp = {}
        if self.headers is None:
            raise Exception("Error")
        for k, v in self.headers:
            k: str
            v: DictObject
            tmp[k] = v.value
        return tmp

    def get_final_body(self):
        # self.body:RequestBody
        return self.body.get_body()

    def has_body(self) -> bool:
        return True if self.body is not None else False

    def send_request(self):
        self.httpManager = urllib3.PoolManager()
        self.response: urllib3.response.HTTPResponse = self.httpManager.request(
            method=self.method,
            url=self.get_final_url(),
            headers=self.get_final_headers(),
            body=self.get_final_body() if self.has_body() else None
        )

    # ----------------------------------------------------------------
    # 从返回值中获取想要的东西.
    def get_response_line(self):
        pass

    def get_response_head(self):
        pass

    def get_response_body(self):
        pass
