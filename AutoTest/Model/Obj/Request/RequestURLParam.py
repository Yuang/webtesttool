from typing import Tuple


class RequestUrlParam:
    def __init__(self):
        pass

    def __iter__(self) -> Tuple[str, str]:
        """这里为了简洁起见，给出的是enumerate后的kv形式数据"""
        pass

    # def get_param(self) -> Iterable:
    #     """这个函数可以改为使用__iter__"""
    #     return {}
