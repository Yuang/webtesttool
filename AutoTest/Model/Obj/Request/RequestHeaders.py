from typing import Tuple


class RequestHeaders:
    def __init__(self):
        raise NotImplementedError

    def __iter__(self) -> Tuple[str, str]:
        raise NotImplementedError

    def __getitem__(self, item) -> None:
        raise NotImplementedError

    def __setitem__(self, key, value) -> None:
        raise NotImplementedError
