class RequestBodyType:
    def __init__(self):
        pass

    def get_content_type(self):
        pass

    def get_body(self):
        pass


class NoneBody(RequestBodyType):
    pass


class FormDataBody(RequestBodyType):
    pass


class XWXXXFormUrlEncoded(RequestBodyType):
    pass


class RawBody(RequestBodyType):
    pass


class BinBody(RequestBodyType):
    pass


class GraphQL(RequestBodyType):
    pass
