from typing import Dict


class DictObject:
    def __init__(self, key=None, value=None, desc=None):
        self.key = key
        self.value = value
        self.desc = desc

    def set_key(self, key):
        self.key = key

    def get_key(self):
        return self.value

    def set_value(self, key):
        self.key = key

    def get_value(self):
        return self.value

    def set_desc(self, desc):
        self.desc = desc

    def get_desc(self):
        return self.desc

    def set_dict(self, m: Dict):
        """这个函数被用于向一个Dict类的对象传入值，使得内部存放形式为{k:MapObj[k,v,d]}"""
        m[self.key] = self
