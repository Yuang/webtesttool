class Result:
    def __init__(self):
        self.right = None
        self.Error = None
        self.row = True  # 这个是T或者F,如果是T,则认为right是有效的,反之则是Error.
        self.content = None

    def r(self, content):
        self.row = True
        self.content = content
        return self

    def w(self, content):
        self.row = False
        self.content = content
        return self
