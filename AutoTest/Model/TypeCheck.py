# from builtins import function

import inspect
from functools import wraps


def type_check(to_be_annotated):
    """
    装饰器，用于进行类型检查。如果要进行装饰，则所有需要检查的参数必须以name=value的形式传入.
    :param to_be_annotated:被修饰的函数
    :return:
    """

    @wraps(to_be_annotated)
    def wrap(*args, **kwargs):
        params = inspect.signature(to_be_annotated).parameters
        for k in params:
            if not isinstance(kwargs[k], params[k].annotation):
                raise TypeError(f"The var {k} should be {params[k].annotation}, "
                                f"but is actually {type(kwargs[k])}, {kwargs[k]}")
        to_be_annotated(*args, **kwargs)

    return wrap
#
#
# def var_type_check(v, t=None):
#     """
#     对变量进行类型检查
#     :param v:
#     :param t:
#     :return:
#     """
#     annotated_type=v.__annotations__
#     actually_type = type(v)
#     assert actually_type==annotated_type
#
# a:int = 10
# var_type_check(a)
